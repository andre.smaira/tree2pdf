# Conversor de árvore de arquivos em PDF

## Requisitos

```
pip3 install python-magic
pip3 install PyLaTeX
sudo apt-get install texlive-pictures texlive-science texlive-latex-extra latexmk 
```

## Execução

```
python3 tree2pdf.py /path/to/tree/root
```