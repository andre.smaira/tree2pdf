import magic
import os
import shutil
import argparse

from pylatex import Document, Package, NoEscape
from pylatex.base_classes import Environment
from pylatex.basic import NewPage
from pylatex.base_classes.command import Command, Options
from pylatex.position import Center


def parse_input():
    parser = argparse.ArgumentParser(description="Check for plagiarism one task of all students")
    parser.add_argument("-i", "--input-path", dest="path", required=True,
                        help="Path to be converted to PDF")
    parser.add_argument("-o", "--output", dest="out", default='out',
                        help="PDF filename")
    parser.add_argument("-r", "--remove", dest="rm", action='store_true', default=False,
                        help="Path to be converted to PDF")

    return parser.parse_args()


class File:
    known_types = {'text/x-python/py': ('script', 'python'),
                   'text/plain/md': ('markdown', 'markdown'),
                   'text/plain/yml': ('script', 'yaml'),
                   'text/plain/sh': ('script', 'bash'),
                   'text/x-c/c': ('script', 'c'),
                   'text/plain': ('script', 'bash'),
                   'text/x-shellscript/sh': ('script', 'bash'),
                   'application/csv/csv': ('csv', 'csv'),
                   'application/pdf/pdf': ('pdf', 'pdf')
                   }

    def __init__(self, path, root):
        self.root = root
        self._path = path
        ext = os.path.splitext(path)[-1]
        self.type = magic.from_file(path, mime=True)+(f'/{ext[1:]}' if len(ext) > 1 else '')
        if self.type not in File.known_types:
            raise TypeError(f"Tipo desconhecido: {self.type}\nArquivo: {path}")

    def latex(self):
        return File.known_types[self.type]

    def content(self):
        with open(self._path) as fp:
            return fp.read()

    def path(self, relative=True):
        if relative:
            return self._path.replace(self.root, '').strip(os.sep)
        return self._path

    def title(self):
        return self.path().replace("tmp", ".").replace('_', '-')


class Minted(Environment):
    packages = [Package('minted')]
    escape = True
    content_separator = "\n\n"


class MarkDown(Environment):
    packages = [Package('markdown', options=['hashEnumerators', 'smartEllipses'])]
    escape = True
    content_separator = "\n\n"


def main():
    args = parse_input()
    folder = args.path
    outf = args.out
    if outf.endswith('.pdf'):
        outf = outf[:-4]
    if os.path.exists(os.path.join('pdfs', outf+'.pdf')):
        if not args.rm:
            print(f'Arquivo {os.path.join("pdfs", outf+".pdf")} já existe!')
            return
        os.remove(os.path.join('pdfs', outf+'.pdf'))
    pdf_tmp = os.path.join('pdfs', 'tmp')
    if os.path.exists(pdf_tmp):
        shutil.rmtree(pdf_tmp)
    shutil.copytree(folder, pdf_tmp)
    folder = pdf_tmp
    fst = True
    tex = Document(geometry_options={"margin": "0.5cm"})
    tex.packages.append(Package('babel', options=['brazil']))
    tex.packages.append(Package('minted'))
    tex.packages.append(Package('pdfpages'))
    tex.packages.append(Package('csvsimple'))
    tex.preamble.append(Command('fvset', 'breaklines=true'))
    tex.packages.append(Package('markdown',
                                options=['fencedCode',
                                         'citations',
                                         'smartEllipses',
                                         'breakableBlockquotes=false']))
    tex.preamble.append(Command('listfiles'))
    for r, ss, ff in os.walk(folder):
        for fn in ff:
            f = File(os.path.join(r, fn), root='pdfs')
            print(f.path())
            text, lang = f.latex()
            if not fst:
                tex.append(NewPage())
            fst = False
            if text != 'pdf':
                with tex.create(Center()):
                    tex.append(Command('large'))
                    tex.append(Command('textbf', f'{f.title()}\n\n'))
            if text == 'script':
                tex.append(Command('inputminted', arguments=[lang, NoEscape(f'\detokenize{{{f.path()}}}')]))
            elif text == 'markdown':
                tex.append(Command('scriptsize'))
                tex.append(Command('markdownInput',
                                   arguments=NoEscape(f'\detokenize{{{f.path()}}}')
                                   )
                           )
            elif text == 'pdf':
                tex.append(Command('includepdf',
                                   options=Options('pages=-',
                                                   NoEscape(r'pagecommand={\textbf{'+f.title()+'}}'),
                                                   'frame=true', 'scale=0.9'),
                                   arguments=NoEscape(f'\detokenize{{{f.path()}}}')))
            elif text == 'csv':
                tex.append(Command('csvautotabular',
                                   options=Options(NoEscape(r'before reading={\catcode`$=12}')),
                                   arguments=NoEscape(f'\detokenize{{{f.path()}}}')))

    os.chdir('pdfs')
    try:
        tex.generate_pdf(outf, compiler_args=['-shell-escape'])
    except UnicodeDecodeError:
        pass
    os.chdir('..')
    for r, ss, ff in os.walk('pdfs'):
        for f in ff:
            if not f.endswith('.pdf'):
                os.remove(os.path.join(r, f))
        if r != 'pdfs':
            shutil.rmtree(r)


if __name__ == '__main__':
    main()
